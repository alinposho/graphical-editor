(defproject editor "0.1.0-SNAPSHOT"
  :description "Graphical editor - OnTheMarket Technical Test"
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]]
  :main ^:skip-aot editor.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
