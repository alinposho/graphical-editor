(ns editor.core
  (:gen-class)
  (:require [editor.functions :refer :all]))

(defn parse-three-args [[X Y C]]
  [(Integer/parseInt X)
   (Integer/parseInt Y)
   C])

(defn parse-four-args [[X Y Z C]]
  [(Integer/parseInt X)
   (Integer/parseInt Y)
   (Integer/parseInt Z)
   C])

(defn -main []
  (println "The editor supports 8 commands:\n1. I M N\u200B. Create a new M x N image with all pixels coloured white (O).\n2. C\u200B. Clears the table, setting all pixels to white (O).\n3. L X Y C\u200B. Colours the pixel (X,Y) with colour C.\n4 .V X Y1 Y2 C\u200B. Draw a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).\n5. H X1 X2 Y C\u200B. Draw a horizontal segment of colour C in row Y between columns X1 and X2 (inclusive).\n6. F X Y C\u200B. Fill the region R with the colour C. R is defined as: Pixel (X,Y) belongs to R. Any other\npixel which is the same colour as (X,Y) and shares a common side with any pixel in R also\nbelongs to this region.\n7. S\u200B. Show the contents of the current image\n8. X\u200B. Terminate the session")
  (println "Please select a command!")
  (loop [line (read-line)
         matrix (atom nil)]
    (when (not= line "X")
      (let [[fcn & arguments] (clojure.string/split line #"\s+")]
        (cond (= fcn "I") (reset! matrix (apply create (map #(Integer/parseInt %) arguments)))
              (= fcn "C") (swap! matrix clear)
              (= fcn "L") (swap! matrix (fn [m] (apply color (cons m (parse-three-args arguments)))))
              (= fcn "V") (swap! matrix (fn [m] (apply vertical (cons m (parse-four-args arguments)))))
              (= fcn "H") (swap! matrix (fn [m] (apply horizontal (cons m (parse-four-args arguments)))))
              (= fcn "F") (swap! matrix (fn [m] (apply fill (cons m (parse-three-args arguments)))))
              (= fcn "S") (do (draw @matrix) (flush)))
        (recur (read-line) matrix)))))

(comment

  (-main)

  (parse-three-args (clojure.string/split "1 2 A" #" "))
  )