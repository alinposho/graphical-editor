(ns editor.functions)

(def ^:const LOWER_BOUND 1)
(def ^:const UPPER_BOUND 250)

(defn in-interval?
  "Tests whether the parameter value is in the [LOWER_BOUND, UPPER_BOUND] interval"
  [value]
  (<= LOWER_BOUND value UPPER_BOUND))

(defn valid-matrix?
  "Returns true if the matrix sizes are within bounds"
  [matrix]
  (and
    (in-interval? (count matrix))
    (apply = (map count matrix))
    (in-interval? (count (matrix 0)))))

(defn create
  "Create an N x M matrix initialized to O"
  [M N]
  {:pre [(in-interval? M)
         (in-interval? N)]}
  (vec (repeatedly N
                   #(vec (repeat M \O)))))


(defn clear
  "Clears the contents of an NxM matrix"
  [matrix]
  {:pre [(valid-matrix? matrix)]}
  (create (count (matrix 0)) (count matrix)))

(defn color
  "Color pixel (X, Y) with color C"
  [matrix X Y C]
  {:pre [(valid-matrix? matrix)
         (in-interval? X)
         (in-interval? Y)]}
  (assoc-in matrix [(dec Y) (dec X)] C))


(defn vertical
  "Color a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).
  Perhaps not the most efficient implementation, but it does the job."
  [matrix X Y1 Y2 C]
  {:pre [(valid-matrix? matrix)
         (in-interval? X)
         (in-interval? Y1)
         (in-interval? Y2)]}
  ;; need to increment since range's second argument is exclusive
  (->> (if (< Y1 Y2) (range Y1 (inc Y2)) (range Y2 (inc Y1)))
       (reduce (fn [new-matrix Y] (color new-matrix X Y C))
               matrix)))

(defn horizontal
  "Color a horizontal segment of colour C in row Y between columns X1 and X2\n(inclusive)\n
  Perhaps not the most efficient implementation, but it does the job."
  [matrix X1 X2 Y C]
  {:pre [(valid-matrix? matrix)
         (in-interval? X1)
         (in-interval? X2)
         (in-interval? Y)]}
  ;; need to increment since range's second argument is exclusive
  (->> (if (< X1 X2) (range X1 (inc X2)) (range X2 (inc X1)))
       (reduce (fn [new-matrix X] (color new-matrix X Y C))
               matrix)))


(defn up [i j] [(dec i) j])
(defn down [i j] [(inc i) j])
(defn left [i j] [i (dec j)])
(defn right [i j] [i (inc j)])

(defn get-color [matrix X Y]
  (get-in matrix [(dec Y) (dec X)]))

(defn color-pixel-and-neighbours
  "Fill the region R with the colour C. R is defined as: Pixel (X,Y) belongs to R. Any other pixel which is the
   same colour as (X,Y) and shares a common side with any pixel in R also belongs to this region."
  ([matrix X Y C]
   {:pre [(valid-matrix? matrix)
          (in-interval? X)
          (in-interval? Y)]}
   (let [curr-color (get-color matrix X Y)
         new-matrix (color matrix X Y C)]
     (reduce (fn [acc-matrix fcn]
               (let [[X1 Y1] (fcn X Y)]
                 (if (not= (get-color acc-matrix X1 Y1) curr-color)
                   acc-matrix
                   (color-pixel-and-neighbours acc-matrix X1 Y1 C))))
             new-matrix
             [up down left right]
             ))))

(def fill color-pixel-and-neighbours)

(defn fill-square [matrix x y offset color]
  (-> matrix
      (vertical (+ x offset) (+ y offset) (- y offset) color)
      (vertical (- x offset) (+ y offset) (- y offset) color)
      (horizontal (- x offset) (+ x offset) (+ y offset) color)
      (horizontal (- x offset) (+ x offset) (- y offset) color)
      ))

(defn radial-fill [matrix x y colors]
  (first
    (reduce (fn [[acc-matrix index] color]
              [(fill-square acc-matrix x y index color) (inc index)])
            [matrix 0]
            colors)))

(defn show-image-contents
  "Draw the graphical editor matrix"
  [matrix]
  (str
    (clojure.string/join "\n"
                         (map clojure.string/join matrix))
    "\n"))

(def draw (comp print show-image-contents))



(comment

  (create 0 250)

  (vec (repeatedly 4 #(vec (repeat 5 \O))))

  (clojure.string/join "\n" [1 2 3 4])

  (-> (create 5 6)
      draw)

  (apply = [1 1 1])

  (assoc-in [[1 1 1]
             [1 1 1]
             [1 1 1]] [1 1] 99)

  (-> (create 5 6)
      (color 2 3 \A)
      (draw))

  (-> (create 5 6)
      (vertical 2 3 4 \W)
      (horizontal 1 4 2 \H)
      (draw))

  (-> (create 2 2)
      (color-pixel-and-neighbours 1 1 \A)
      (draw))

  (-> (create 5 6)
      (vertical 2 3 4 \W)
      (horizontal 1 4 2 \H)
      (color-pixel-and-neighbours 1 1 \A)
      #_(draw))

  (draw (fill-square [[1 1 1 1 1]
                      [1 1 1 1 1]
                      [1 1 1 1 1]
                      [1 1 1 1 1]
                      [1 1 1 1 1]]
                     3 3
                     0
                     \G))


  (-> (create 9 9)
      (radial-fill 5 5 [\A \b \C \d \E])
      (draw))

  )
