(ns editor.functions-test
  (:require [clojure.test :refer :all]
            [editor.functions :refer :all]))


(deftest create-test
  (testing "The create function parameter bounds"
    (is (thrown? AssertionError (create 0 5)))
    (is (thrown? AssertionError (create 251 5)))
    (is (thrown? AssertionError (create 250 0)))
    (is (thrown? AssertionError (create 1 251))))

  (testing "The create function result"
    (is (= [[\O]] (create 1 1)))
    (is (= [[\O \O \O]
            [\O \O \O]]
           (create 3 2)))))

(deftest clear-test
  (testing "The create function parameter bounds"
    (is (thrown? AssertionError (clear [[1] [1 2]])))
    (is (thrown? AssertionError (clear [])))
    (is (thrown? AssertionError (clear nil))))

  (testing "The clear function result"
    (is (= [[\O]] (clear [[1]])))
    (is (= [[\O \O \O]
            [\O \O \O]]
           (clear [[1 2 2]
                   [\1 \a "bubu"]])))))

(deftest color-test
  (testing "The color function parameter bounds"
    (is (thrown? AssertionError (color [[1] [1 2]] 0 0 "bubu")))
    (is (thrown? AssertionError (color [] 1 1 \r))))

  (testing "The color function result"
    (is (= [["bubu"]] (color [[\O]] 1 1 "bubu")))
    (is (= [[\O \O \O]
            [\O \O 4]]
           (color (create 3 2)
                  3 2
                  4)))))

(deftest vertical-test
  (testing "The vertical function parameter bounds"
    (is (thrown? AssertionError (vertical [[1 3] [1 2]] 1 2 0 "bubu")))
    (is (thrown? AssertionError (vertical nil 1 2 0 "bubu"))))
  (testing "The vertical function result"
    (is (= [["bubu"]] (vertical [[\O]] 1 1 1 "bubu")))
    (is (= [[99 \O \O]
            [99 \O \O]]
           (vertical (create 3 2)
                     1
                     1 2
                     99)))
    (is (= [[\O \O \O]
            [99 \O \O]
            [99 \O \O]]
           (vertical (create 3 3)
                     1
                     3 2
                     99)))
    ))

(deftest horizontal-test
  (testing "The horizontal function parameter bounds"
    (is (thrown? AssertionError (horizontal [[1 3] [1 2]] 0 2 1 "bubu")))
    (is (thrown? AssertionError (horizontal nil 1 2 0 "bubu"))))
  (testing "The horizontal function result"
    (is (= [["bubu"]] (horizontal [[\O]] 1 1 1 "bubu")))
    (is (= [[\O \O \O]
            [99 99 99]
            [\O \O \O]]
           (horizontal (create 3 3)
                       1 3
                       2
                       99)))
    (is (= [[\O \O \O]
            [\O 99 99]
            [\O \O \O]]
           (horizontal (create 3 3)
                       3 2
                       2
                       99)))
    ))


(deftest color-pixel-and-neighbours-test
  (testing "The color-pixel-and-neighbours function parameter bounds"
    (is (thrown? AssertionError (color-pixel-and-neighbours nil 1 2 "bubu"))))
  (testing "The color-pixel-and-neighbours function result"
    (is (= [["bubu"]] (color-pixel-and-neighbours [[\O]] 1 1 "bubu")))
    (is (= [[99 99 99]
            [99 99 99]
            [99 99 99]]
           (color-pixel-and-neighbours (create 3 3)
                                       1 3
                                       99)))
    (is (= [[\A \A \A \A \A]
            [\H \H \H \H \A]
            [\A \W \A \A \A]
            [\A \W \A \A \A]
            [\A \A \A \A \A]
            [\A \A \A \A \A]]
           (-> (create 5 6)
               (vertical 2 3 4 \W)
               (horizontal 1 4 2 \H)
               (color-pixel-and-neighbours 1 1 \A))))
    (is (= [[\A \A \A \A \H]
            [\H \H \H \H \g]
            [\A \W \g \g \g]
            [\A \W \g \g \g]
            [\X \X \X \X \X]
            [\A \A \A \A \A]]
           (color-pixel-and-neighbours [[\A \A \A \A \H]
                                        [\H \H \H \H \A]
                                        [\A \W \A \A \A]
                                        [\A \W \A \A \A]
                                        [\X \X \X \X \X]
                                        [\A \A \A \A \A]]
                                       3 3
                                       \g
                                       )))))


(deftest fill-square-test
  (testing "The fill-square function result"
    (is (= [[99 99 99]
            [99 \A 99]
            [99 99 99]]
           (color-pixel-and-neighbours (create 3 3)
                                       1 3
                                       99)))
    (is (= [[\A \A \A \A \A]
            [\A \H \H \H \A]
            [\A \W \A \A \A]
            [\A \W \A \A \A]
            [\A \A \A \A \A]]
           (-> (create 5 6)
               (vertical 2 3 4 \W)
               (horizontal 1 4 2 \H)
               (color-pixel-and-neighbours 1 1 \A))))
    (is (= [[\A \A \A \A \H]
            [\H \H \H \H \g]
            [\A \W \g \g \g]
            [\A \W \g \g \g]
            [\X \X \X \X \X]
            [\A \A \A \A \A]]
           (color-pixel-and-neighbours [[\A \A \A \A \H]
                                        [\H \H \H \H \A]
                                        [\A \W \A \A \A]
                                        [\A \W \A \A \A]
                                        [\X \X \X \X \X]
                                        [\A \A \A \A \A]]
                                       3 3
                                       \g
                                       )))))

(deftest radial-fill-test
  (testing "The color-pixel-and-neighbours function parameter bounds"
    (is (thrown? AssertionError (color-pixel-and-neighbours nil 1 2 "bubu"))))
  (testing "The color-pixel-and-neighbours function result"
    (is (= [["bubu"]] (color-pixel-and-neighbours [[\O]] 1 1 "bubu")))
    (is (= [[99 99 99]
            [99 99 99]
            [99 99 99]]
           (color-pixel-and-neighbours (create 3 3)
                                       1 3
                                       99)))
    (is (= [[\A \A \A \A \A]
            [\H \H \H \H \A]
            [\A \W \A \A \A]
            [\A \W \A \A \A]
            [\A \A \A \A \A]
            [\A \A \A \A \A]]
           (-> (create 5 6)
               (vertical 2 3 4 \W)
               (horizontal 1 4 2 \H)
               (color-pixel-and-neighbours 1 1 \A))))
    (is (= [[\A \A \A \A \H]
            [\H \H \H \H \g]
            [\A \W \g \g \g]
            [\A \W \g \g \g]
            [\X \X \X \X \X]
            [\A \A \A \A \A]]
           (color-pixel-and-neighbours [[\A \A \A \A \H]
                                        [\H \H \H \H \A]
                                        [\A \W \A \A \A]
                                        [\A \W \A \A \A]
                                        [\X \X \X \X \X]
                                        [\A \A \A \A \A]]
                                       3 3
                                       \g
                                       )))))


(deftest show-image-contents-test
  (testing "The show-image-contents result"
    (is (= "OOOOO\nOOOOO\nOOOOO\nOOOOO\nOOOOO\nOOOOO\n"
           (show-image-contents (create 5 6))))))

